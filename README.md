### Overzicht

| type | description
|-----------------------------------------------------------------------------------|---|
| ![img](https://img.shields.io/badge/-direct-success.svg?style=flat-square)        | Hoge prioriteit/Direct te implementeren |
| ![img](https://img.shields.io/badge/-short-yellowgreen.svg?style=flat-square)     | Korte termijn |
| ![img](https://img.shields.io/badge/-long-orange.svg?style=flat-square)           | Lange termijn |
| ![img](https://img.shields.io/badge/-partial-lightgrey.svg?style=flat-square)     | Gedeeltelijk implementeren |

|                                                                                               | estimate          | name                  | description |
|-----------------------------------------------------------------------------------------------|-------------------|-----------------------|-------------|
| ![img](https://img.shields.io/badge/common-direct-success.svg?style=flat-square)              | `3 hours`         | [Git](#Git)           | Version control system. Bitbucket cloud or Bitbucket server.  |
| ![img](https://img.shields.io/badge/common-short-yellowgreen.svg?style=flat-square)           | `30 minutes`      | [Floobits](#Floobits) | Task Runner |
| ![img](https://img.shields.io/badge/common-short-yellowgreen.svg?style=flat-square)           | `2 hours`         | [Gulp/grunt/phing](#Gulp/grunt/phing) | Task Runner |
| ![img](https://img.shields.io/badge/common-long-orange.svg?style=flat-square)                 | `72 hours`        | [Jenkins](#Jenkins)   | Continues Integration/Deployment |
| ![img](https://img.shields.io/badge/backend-direct-success.svg?style=flat-square)             | `30 minutes`      | [Composer](#Composer) | PHP Package Manager & Autoloader |
| ![img](https://img.shields.io/badge/backend-short-yellowgreen.svg?style=flat-square)          | `1 hour`          | [Sentry](#Sentry)     | Error tracking |
| ![img](https://img.shields.io/badge/backend-partial-lightgrey.svg?style=flat-square)          | `4 hours`         | [PHPUnit](#PHPUnit)   | Unit Testing |
| ![img](https://img.shields.io/badge/frontend-short-yellowgreen.svg?style=flat-square)         | `3 hours`         | [NPM](#NPM)           | JS Package Manager |
| ![img](https://img.shields.io/badge/frontend-long-orange.svg?style=flat-square)               | `24 hours`        | [Webpack](#Webpack)   | Module Bundler |


### Details


<a name="Git"></a>
#### Git
_PHPStorm integratie/plugin beschikbaar_

Versiebeheersysteem die gebruik maakt van een client-server methode 

- Workflow voorbeeld:
    - Een online (private) repository (`remote`) 
    - `git clone` Developers kopieeren de online repository naar een lokale (op harde schijf van de computer) repository
    - Developers maken veranderingen in de code 
    - `git commit` Veranderingen opslaan als een soort snapshot/herstelpunt.
    - `git push` Veranderingen uploaden naar de online repository (`remote`), zodat deze gedeeld worden met andere developers.   
- Online repository:
    - `repository` is een code project. Het is mogelijk om users read, write en/of admin rechten toe te kennen.  
    - `public repository` is een repositorie die voor iedereen te bekijken is. 
    - `private repository` is alleen te bekijken voor users die rechten hebben.
    - `organisation/team` is een groep die repositories bevat. 
    - [Github](https://github.com) 
        - Het bekenste/meest gebruikte voorbeeld van een online repository. 
        - Het is gratis voor `public repositories`/`open source organisations`. 
        - `organisations` met `private repositories` zijn betaald, prijzen gebaseerd op hoeveelheid users er in de `organisation` zitten. 
        - Vanaf: $25/month voor 5 users  
        - Elke extra user: $9/month
        - Is voornamelijk interesant voor open-source code.
    - [Bitbucket Cloud](https://bitbucket.org) 
        - Is populair vanwege gratis `private repositories`
        - Private repositories zijn gratis  
        - Organisations tot 5 users zijn gratis. Boven 5 users betaald. Prijzen gebaseerd op hoeveelheid users er in de `organisation` zitten.
        - Vanaf: $10/month voor 5 users
        - Elke extra user: $2/month
        - Is gericht op bedrijven en interesant private code.


<a name="Floobits"></a>
#### Floobits
Pair-programming tool.
[Verschillende voorbeelden](https://floobits.com/)

| Developer/Computer A | Developer/Computer B |
|---|---|
| ![](https://floobits.com/static/images/v2/hangout_sublime.gif)  | ![](https://floobits.com/static/images/v2/hangout_hangout.gif) |

    
<a name="Gulp/grunt/phing"></a>    
#### Gulp/grunt/phing
_PHPStorm integratie/plugin beschikbaar_

Een task runner voert bepaalde taken uit. Bijvoorbeeld:
- Tests uitvoeren
- SCSS naar CSS omzetten
- Security checks
- Git taken
- Changelog genereren
- Documentatie genereren

<a name="Jenkins"></a>
#### Jenkins
_PHPStorm integratie/plugin beschikbaar_

[Jenkins](https://jenkins.io) is een Continues Integration/Deployment tool. [Ga naar voorbeeld](https://jenkins.radic.ninja/blue). 
Jenkins kan **alles**. Enkele voorbeelden van functionaliteit:
- Jobs uitvoeren als er een git commit is gepushed naar een online repository (bv bitbucket). [Voorbeeld](https://jenkins.radic.ninja/blue/organizations/jenkins/codex%2Fdevelop%2Ftest/detail/test/110/pipeline)
- Unit tests uitvoeren [Voorbeeld](https://jenkins.radic.ninja/blue/organizations/jenkins/codex%2Fdevelop%2Ftest/detail/test/110/tests)
- Unit test coverage publiceren [Voorbeeld](https://jenkins.radic.ninja/job/codex/job/develop/job/test/110/Code_20Coverage/)
- Unit tests uitvoeren en indien niet successvol de git commit niet accepteren 
- De applicatie deployen naar een development of distributie server [Voorbeeld](https://jenkins.radic.ninja/blue/organizations/jenkins/codex.radic.ninja%2Fdeploy/detail/deploy/44/pipeline/7)
- En nog veel, veel, veeeeel meer  
  
<a name="Composer"></a>  
#### Composer
_PHPStorm integratie/plugin beschikbaar_
[Composer](https://getcomposer.org) is een Package Manager en PSR Autoloader voor PHP.
- Package Manager
    - Installeert libraries vanuit de public package repository [Packagist](https://packagist.org)
    - Installeert private libraries. Deze kunnen op een prive package repository gehost worden. voorbeeld: [packages.radic.ninja](https://packages.radic.ninja)
- PSR Autoloading
    - Genereert PSR Autoloading volgens de PSR-x Autoloading specificaties van [PHP Framework Interop Group](https://www.php-fig.org/)
    - Zorgt voor namespaced class autoloading. 

##### Voorbeeld  
_`project/composer.json`_
```json
{
    "require": {
        "codeigniter4/codeigniter4": "v4.0.0-beta-3"
    },
    "autoload": {
        "psr-4": {
            "WMO\\Core\\": "src/"
        }
    }
}
```  
_`project/src/MyClass.php`_  
```php
namespace WMO\Core;

class MyClass {}
```
  
_`project/src/Models/User.php`_
```php
namespace WMO\Core\Models;

use CodeIgniter\Model;
use WMO\Core\MyClass;

class User extends Model {
    protected $my;
    protected $table      = 'users';
    protected $primaryKey = 'id';
    protected $validationRules    = [
        'username'     => 'required|alpha_numeric_space|min_length[3]',
        'email'        => 'required|valid_email|is_unique[users.email]',
        'password'     => 'required|min_length[8]',
        'pass_confirm' => 'required_with[password]|matches[password]'
    ];
    protected $validationMessages = [
        'email'        => [
                'is_unique' => 'Sorry. That email has already been taken. Please choose another.'
        ]
    ];
    
    __construct(){
        parent::__construct();
        $this->my = new MyClass();
    }
}
```  


<a name="Sentry"></a>  
#### Sentry
Open-source error tracking that helps developers monitor and fix crashes in real time.


  
<a name="PHPUnit"></a>  
#### PHPUnit 
_PHPStorm integratie/plugin beschikbaar_

- Verbetert de kwaliteit van de code. 
- Identificeert bugs/defects 
- Als je tests schrijft voordat je de code daadwerkelijk schrijft, denk je beter na over het probleem. 
- Ervoor zorgen dat de applicatie nog steeds correct werkt nadat programmeurs code hebben veranderd
- De opvatting dat tests schrijven meer tijd kost is fout. **Tests schrijven bespaart tijd**. 
  Omdat de bugs/fouten vroeg/sneller worden gevonden, helpt het de kosten van bugfixes/foutencorrecties te verminderen. 
  Stel je zich eens de kosten voor van een bug die tijdens de latere ontwikkelingsstadia gevonden word. 
  Deze bugs zijn meestal het resultaat zijn van veel veranderingen in de code en het is niet precies duidelijk wat de bug heeft veroorzaakt.
 

<a name="NPM"></a>
#### NPM  
_PHPStorm integratie/plugin beschikbaar_

[NPM](https://www.npmjs.com/) is een Package Manager voor Javascript/HTML/CSS. 
- Installeert libraries vanuit de public package repository [NPMJS](https://www.npmjs.com)

_[package.json]_
```json
{
    "dependencies": {
        "gulp": "^4.0.0",
        "jquery": "^3.0.0"
    }
}
```

<a name="Webpack"></a>
#### Webpack  
